package opinionbureau.com.omnible.myutils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by macmine on 15/03/18.
 */

public class MyUtils {

    public static String LoginPREFERENCES = "UserLogin";


    public static void setMacAddress(Context context, String mac) {
        SharedPreferences sharedpreferences2 = context.getSharedPreferences(
                MyUtils.LoginPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences2.edit();
        editor.putString("macaddress", mac).commit();
    }

    public static String getMacAddress(Context context) {
        SharedPreferences sharedpreferences2 = context.getSharedPreferences(
                MyUtils.LoginPREFERENCES, Context.MODE_PRIVATE);
        String data = sharedpreferences2.getString("macaddress", "");

        return data;
    }

}
