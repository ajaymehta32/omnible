package opinionbureau.com.omnible;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import opinionbureau.com.omnible.adapter.ScanDeviceAdapter;
import opinionbureau.com.omnible.ble.BLEDevice;
import opinionbureau.com.omnible.ble.GattAttributes;
import opinionbureau.com.omnible.myutils.MyUtils;
import opinionbureau.com.omnible.utils.CRCUtil;
import opinionbureau.com.omnible.utils.CommandUtil;

public class MainActivity extends Activity implements OnItemClickListener, View.OnClickListener {

    String messageee="";
    public static final int HANDLER_CLOSE = 2;
    public static final int HANDLER_OPEN = 3;
    public static final int HANDLER_GETKEY = 1;
    private static final String TAG = "MainActivity";
    // Stops scanning after 10 seconds
    private static final long SCAN_PERIOD = 10000; // 10s for scanning
    private static final String ACTION_CONNECT_BLE = "opinionbureau.com.omnible.ACTION_CONNECT_BLE";
    private static final String ACTION_DISCONNECT = "opinionbureau.com.omnible.ACTION_DISCONNECT";
    private static final String ACTION_GET_LOCK_KEY = "opinionbureau.com.omnible.ACTION_GET_LOCK_KEY";
    private static final String ACTION_LOCK_CLOSE = "opinionbureau.com.omnible.ACTION_LOCK_CLOSE";
    private static final String ACTION_BLE_LOCK_OPEN_STATUS = "opinionbureau.com.omnible.ACTION_BLE_LOCK_OPEN_STATUS";
    private Handler mHandler = new Handler();
    private boolean mScanning;
    private ListView lvScanDevice; // scan device list
    private ScanDeviceAdapter scanAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBLEGatt;
    private BluetoothGattCharacteristic mBLEGCWrite;
    private BluetoothGattCharacteristic mBLEGCRead;
    private BluetoothAdapter.LeScanCallback mLescanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.d("sendopencommandble", "rssi " + rssi + " device " + device.getAddress()+" "+device.getName()+" "+scanRecord);
            if(MyUtils.getMacAddress(MainActivity.this).equalsIgnoreCase( String.valueOf(device.getAddress()))) {
                Log.d("gotaddress",device.getAddress()+"");
                scanLeDevice(false);
                mBLEGatt = device.connectGatt(MainActivity.this, false, mGattCallback);

             //   sendOpenCommand();

            }

            final BLEDevice bleDevice = new BLEDevice(device, rssi);
            final String deviceName = device.getName();
          //  if (scanAdapter != null && ("OmniLock".equals(deviceName))) {  //we showing device not based on names but all the mac address...
            if (scanAdapter != null ) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanAdapter.addBLEDevice(bleDevice);
                        scanAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };
    private byte bleCKey = 0;
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            if (newState == BluetoothProfile.STATE_CONNECTED) {

                sendLocalBroadcast(ACTION_CONNECT_BLE);
                gatt.discoverServices();
            } else {
                Log.i(TAG, "onConnectionStateChange: ble disconnection");

                sendLocalBroadcast(ACTION_DISCONNECT);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            BluetoothGattService bleGattService = gatt.getService(GattAttributes.UUID_SERVICE);
            mBLEGCWrite = bleGattService.getCharacteristic(GattAttributes.UUID_CHARACTERISTIC_WRITE);


            Log.d("ajaymehta",status +" "+"bluetooth discovered");

            //1.0783b0 3e-8535-b5a0-7140-a304d2495cb8
            //1. get read characteristic
            mBLEGCRead = bleGattService.getCharacteristic(GattAttributes.UUID_CHARACTERISTIC_READ);
            //2. descriptor 00002902-0000-1000-8000-00805f9b34fb
            //2. set descriptor notify
            BluetoothGattDescriptor descriptor = mBLEGCRead.getDescriptor(GattAttributes.UUID_NOTIFICATION_DESCRIPTOR);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBLEGatt.writeDescriptor(descriptor);
            Log.d("sendopencommand",mBLEGCWrite+" "+mBLEGCRead);
            Log.d("ajaymehta",status +" "+"bluetooth discovered");
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

            //3. set characteristic notify
            gatt.setCharacteristicNotification(mBLEGCRead, true);

            Log.i(TAG, "onDescriptorWrite: request the ope key");
            int uid = 1; // user login id

            byte[] crcOrder = CommandUtil.getCRCKeyCommand2();
            Log.i(TAG, "onDescriptorWrite: GET KEY COMM=" + getCommForHex(crcOrder));
            mBLEGCWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            mBLEGCWrite.setValue(crcOrder);
            mBLEGatt.writeCharacteristic(mBLEGCWrite);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] values = characteristic.getValue();
            Log.i(TAG, "onCharacteristicChanged: values=" + getCommForHex(values));
            int start = 0;
            int copyLen = 0;
            for (int i = 0; i < values.length; i++) {
                if ((values[i] & 0xFF) == 0xFE) {
                    start = i;
                    int randNum = (values[i + 1] - 0x32) & 0xFF; //BF
                    int len = ((values[i + 4]) & 0xFF) ^ randNum;
                    copyLen = len + 7; //16+
                    break;
                }
            }
            if (copyLen == 0) return;
            byte[] real = new byte[copyLen];
            System.arraycopy(values, start, real, 0, copyLen);


            byte[] command = new byte[values.length - 2];
            command[0] = values[0]; // 包头
            if (CRCUtil.CheckCRC(real)) {
                // crc校验成功
                byte head = (byte) (real[1] - 0x32);
                command[1] = head;
                for (int i = 2; i < real.length - 2; i++) {
                    command[i] = (byte) (real[i] ^ head);
                }
                handCommand(command);
            } else {
                // CRC校验失败
                Log.i(TAG, "onCharacteristicChanged: CRC校验失败");
            }
            Log.i("oncharactersticChanged","called");
        }
    };
    private TextView txInfo;
    private Button btnScan;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_CLOSE:
                    sendLockResp();
                    Toast.makeText(MainActivity.this, R.string.received_lock, Toast.LENGTH_LONG).show();
                    Log.d("ajaymehta","Recieved lock");
                    break;
                case HANDLER_OPEN:
                    sendOpenResponse();
                    break;
                case HANDLER_GETKEY:
                    txInfo.setVisibility(View.VISIBLE);
                    txInfo.setText(R.string.info);
                    Log.d("ajaymehta","connected ble device,can unlock");
                    break;
            }
        }
    };
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_CONNECT_BLE.equals(intent.getAction())) {
                lvScanDevice.setVisibility(View.GONE);

            } else if (ACTION_LOCK_CLOSE.equals(intent.getAction())) {
                handler.sendEmptyMessage(HANDLER_CLOSE);
            } else if (ACTION_GET_LOCK_KEY.equals(intent.getAction())) {
                handler.sendEmptyMessage(HANDLER_GETKEY);

            } else if (ACTION_DISCONNECT.equals(intent.getAction())) {
                scanAdapter.clearDevice();
                lvScanDevice.setVisibility(View.VISIBLE);
                txInfo.setVisibility(View.GONE);
            } else if (ACTION_BLE_LOCK_OPEN_STATUS.equals(intent.getAction())) {


                int status = intent.getIntExtra("status", 0);

            //    Toast.makeText(getApplicationContext(),status+" " +"lock open status",Toast.LENGTH_SHORT).show();

                Log.d("ajaymehta",status +"Lock open status");

                long timestamp = intent.getLongExtra("timestamp", 0L);
                Log.i(TAG, "onReceive: status=" + status);
                Log.i(TAG, "onReceive: timestamp=" + timestamp);
                handler.sendEmptyMessage(HANDLER_OPEN);

            }
        }
    };

    private String getCommForHex(byte[] values)  {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < values.length; i++) {
            sb.append(String.format("%02X,", values[i]));
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }

    Message message = null;

    private void handCommand(byte[] command) {
        Log.i(TAG, "handCommand: ord= " + String.format("0x%02X", command[3]));
        switch (command[3]) {


            case 0x11:
                // get key
                messageee = "scanned you can unlock";
                message = mHandler.obtainMessage(command[3]);
                message.sendToTarget();
                handKey(command);
                break;
            case 0x22:
                // lock
                messageee = "lock closed";
                message = mHandler.obtainMessage(command[3]);
                message.sendToTarget();
                handLockClose(command);
                break;
            case 0x21:
                messageee = "lock open";
                message = mHandler.obtainMessage(command[3]);
                message.sendToTarget();
                handLockOpen(command);
                break;
            case 0x31:
                messageee = "0x31";
                message = mHandler.obtainMessage(command[3]);
                message.sendToTarget();
              Log.d("thecommand","31 callled");
                break;
        }
    }

    private void handKey(byte[] command) {
        bleCKey = command[5];
        Log.i(TAG, "handKey: key=0x" + String.format("%02X", bleCKey));
        sendLocalBroadcast(ACTION_GET_LOCK_KEY);
    }

    private void handLockClose(byte[] command) {
        int status = command[5];
        long timestamp = ((command[6] & 0xFF) << 24) | ((command[7] & 0xff) << 16) | ((command[8] & 0xFF) << 8) | (command[9] & 0xFF);
        int runTime = ((command[10] & 0xFF) << 24) | ((command[11] & 0xff) << 16) | ((command[12] & 0xFF) << 8) | (command[13] & 0xFF);


        Log.i(TAG, "handLockClose: status=" + status);
        Log.i(TAG, "handLockClose: timestamp=" + timestamp);
        Log.i(TAG, "handLockClose: runTime=" + runTime);

     //   Toast.makeText(getApplicationContext(),status+" " +"Lock close",Toast.LENGTH_SHORT).show();
        Log.d("ajaymehta",status +"Lock close");

        Intent intent = new Intent(ACTION_LOCK_CLOSE);
        intent.putExtra("runTime", runTime);
        intent.putExtra("timestamp", timestamp);
        intent.putExtra("status", status);
        sendLocalBroadcast(intent);
    }

    private void handLockOpen(byte[] command) {
        int status = command[5];

    //    Toast.makeText(getApplicationContext(),status +" " +" " +"Lock open",Toast.LENGTH_SHORT).show();

        Log.d("ajaymehta",status +"Lock open");

        long timestamp = ((command[6] & 0xFF) << 24) | ((command[7] & 0xff) << 16) | ((command[8] & 0xFF) << 8) | (command[9] & 0xFF);
        Intent intent = new Intent(ACTION_BLE_LOCK_OPEN_STATUS);
        intent.putExtra("status", status);
        intent.putExtra("timestamp", timestamp);
        sendLocalBroadcast(intent);

    }

    private void sendLockResp() {
        int uid = 1;
        byte[] crcOrder = CommandUtil.getCRCLockCommand(bleCKey);
        Log.i(TAG, "sendOpenResponse: 上锁回复=" + getCommForHex(crcOrder));
        mBLEGCWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBLEGCWrite.setValue(crcOrder);
        mBLEGatt.writeCharacteristic(mBLEGCWrite);
        Log.d("sendopencommand4",mBLEGCWrite +" "+mBLEGatt);

    }

    public void sendOpenResponse() {
        byte[] crcOrder = CommandUtil.getCRCOpenResCommand(bleCKey);
        Log.i(TAG, "sendOpenResponse: 开锁回复=" + getCommForHex(crcOrder));
        mBLEGCWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBLEGCWrite.setValue(crcOrder);
        mBLEGatt.writeCharacteristic(mBLEGCWrite);
    }

    TextView statusText;
    Button statusButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusText = (TextView) findViewById(R.id.status_info) ;
        statusButton = (Button) findViewById(R.id.button_status) ;

        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            byte[] gotKey =    CommandUtil.getLockStatusCommand(bleCKey);
                Log.i(TAG, "getCRCKeyCommand2: 原始"+getCommForHex(gotKey));

                statusText.setText( "getCRCKeyCommand2: 原始"+getCommForHex(gotKey));
            }
        });


        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                // This is where you do your work in the UI thread.
                // Your worker tells you in the message what to do.

                Log.d("messageaa",message+"");

                Toast.makeText(MainActivity.this, messageee+" "+ message,Toast.LENGTH_LONG).show();
            }
        };


        txInfo = (TextView) findViewById(R.id.tx_info);
        txInfo.setVisibility(View.GONE);
        lvScanDevice = (ListView) findViewById(R.id.lv_device);
        scanAdapter = new ScanDeviceAdapter(this);
        lvScanDevice.setAdapter(scanAdapter);
        lvScanDevice.setOnItemClickListener(this);

        findViewById(R.id.btn_open).setOnClickListener(this);
        btnScan = (Button) findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(this);

        initBLE();


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_CONNECT_BLE);
        intentFilter.addAction(ACTION_LOCK_CLOSE);
        intentFilter.addAction(ACTION_GET_LOCK_KEY);
        intentFilter.addAction(ACTION_DISCONNECT);
        intentFilter.addAction(ACTION_BLE_LOCK_OPEN_STATUS);

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    private void initBLE() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent, 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPermission();
//        scanLeDevice(true);


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mScanning) {
            scanLeDevice(false);
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    private void sendLocalBroadcast(String action) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(action));
    }

    private void sendLocalBroadcast(Intent intent) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void scanLeDevice(final boolean enable) {

        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLescanCallback);
                    btnScan.setText(R.string.scan);
                }
            }, SCAN_PERIOD);
            mScanning = true;
            mBluetoothAdapter.startLeScan(mLescanCallback);
            btnScan.setText(R.string.scanning);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLescanCallback);
            btnScan.setText(R.string.scan);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BLEDevice bleDevice = (BLEDevice) parent.getItemAtPosition(position);
        MyUtils.setMacAddress(MainActivity.this, bleDevice.getDevice().getAddress());
        Log.i(TAG, "onItemClick: MAC=" + bleDevice.getDevice().getAddress());

        scanLeDevice(false);
        mBLEGatt = bleDevice.getDevice().connectGatt(this, false, mGattCallback);
    }

    @Override
    public void onClick(View v) {
        // send  unlock command
        switch (v.getId()) {
            case R.id.btn_open:
                sendOpenCommand();
                break;
            case R.id.btn_scan:
                if (!mScanning) {
                    scanAdapter.clearDevice();
                    scanLeDevice(true);
                }
                break;
        }
    }

    private void sendOpenCommand() {
        if (mBLEGatt != null && mBLEGCWrite != null) {
            int uid = 1;
            long timestamp = System.currentTimeMillis() / 1000;
            byte[] crcOrder = CommandUtil.getCRCOpenCommand(uid, bleCKey, timestamp);
            Log.i(TAG, "sendOpenCommand: openComm=" + getCommForHex(crcOrder));
            mBLEGCWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            mBLEGCWrite.setValue(crcOrder);
            mBLEGatt.writeCharacteristic(mBLEGCWrite);
            Log.d("sendopencommand2",mBLEGatt+" "+mBLEGCWrite);
        } else {
            Toast.makeText(this, R.string.connect_tip, Toast.LENGTH_SHORT).show();
            Log.d("sendopencommand3",mBLEGatt+" "+mBLEGCWrite);
            Log.d("ajaymehta","Must connected BLE device");
        }

    }

    private void getPermission() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // 检测定位权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 2) {
            // 请求定位权限
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //
            } else {
                showDialog(getString(R.string.main_permissions_location));
            }
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(R.string.main_dialog_tip);
        builder.setPositiveButton(R.string.main_dialog_submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
